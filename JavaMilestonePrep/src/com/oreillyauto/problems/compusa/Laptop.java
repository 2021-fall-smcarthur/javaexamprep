package com.oreillyauto.problems.compusa;

class Laptop {
    
    public String SKU = null;
    public String serialNumber = null;
    public static Integer laptopCount = 0;
    
    public Laptop(String SKU, String serialNumber) {
        this.SKU = SKU;
        this.serialNumber = serialNumber;
    }
    public String getSKU() {
        return this.SKU;
    }
    public String getSerialNumber() {
        return this.serialNumber;
    }
    public static int getLaptopCount() {
        return laptopCount;
    }
    public static void setLaptopCount(int laptops) {
        laptopCount = laptops;
    }
}
