package com.oreillyauto.problems.compusa;

class TransactionException extends Exception{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public String errorMessage = null;
    public String errorCode = null;
    TransactionException(String errorMessage, String errorCode){
        
        super(errorMessage);
        
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
 
       // throw new TransactionException(errorMessage, errorCode, 0);
        /* if (errorCode.equals("SERIAL_NUMBER_MISSING")) {
        
            this.errorMessage = "Serial number Missing.";
        }else if(errorCode.equals("OUT_OF_STOCK")) {
            this.errorMessage =  "Out of stock.";
        }else if(errorCode.equals("INVALID_COUNT")) {
            this.errorMessage = "Count should be greater than zero."; 
        }else {
           this.errorMessage = errorMessage; 
        }*/
    }
    public TransactionException(String errorMessage, String errorCode, int count) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
  
    public String getErrorCode() {
        return this.errorCode;
    }
    public String getErrorMessage() {
        return this.errorMessage;
    }
}
