package com.oreillyauto.problems.compusa;

import java.io.IOException;
import java.util.ArrayList;

class LaptopTransaction{
    public static ArrayList<Laptop> laptopList = new ArrayList<Laptop>();
    
    public static String addLaptop(Laptop laptop, int count) throws TransactionException {
        
        if (laptop.getSerialNumber()!=null) {
            if (count > 0) {
                laptopList.add(laptop);
                Laptop.setLaptopCount(Laptop.getLaptopCount()+1);
                return "Laptops successfully added.";
            }else {
                throw new TransactionException("Count should be greater than zero.", "INVALID_COUNT");
             }
            
             
        }
        else {
            throw new TransactionException("Serial Number Missing.", "SERIAL_NUMBER_MISSING");
                
        }
    }
    public static String sellLaptop(Laptop laptop, int count) throws TransactionException {
        if (laptopList.contains(laptop)) {
            if (count > 0) {
                laptopList.remove(laptop);
                return "Laptops successfully sold.";
            }else {
                throw new TransactionException("Count should be greater than zero.", "INVALID_COUNT");
             }
        }else {
            throw new TransactionException("Out of stock.", "OUT_OF_STOCK");
           }
    }
}
