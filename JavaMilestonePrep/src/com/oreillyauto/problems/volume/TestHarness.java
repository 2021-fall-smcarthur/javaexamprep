package com.oreillyauto.problems.volume;

/**
 * - You are given an abstract class (Volume) with its fields and methods declared. 
 * - The Volume class contains member variables called gallons and ounces. 
 * - Implement the four methods in Volume.java in a class called VolumeImplementation
 * - DO NOT use an access modifier for your class!
 * - The test harness will pass values (gallons and ounces) to your implementation
 * - Your implementation should contain a method called "getVolumeComparison()"  
 * - Return which of the distances is greater, if there is one. 
 * - The test harness already knows which is greater. If you report the greater volume
 *   you will pass the test case scenario.
 * 
 * - Uncomment the main method and ensure that it runs.
 * - The expected output is:
 *    Second volume is greater.
 * 
 * @author jbrannon5
 *
 */
public class TestHarness {
    public static void main(String[] args) {
        Volume vol1 = new VolumeImplementation();
        Volume vol2 = new VolumeImplementation();
        
        int gallons1 = 1;
        float ounces1 = 32;
        
        int gallons2 = 1;
        float ounces2 = 64;
        
        vol1.setGallonsAndOunces(gallons1, ounces1);
        vol2.setGallonsAndOunces(gallons2, ounces2);
        
        String response = vol1.getVolumeComparison(vol2);
        
        if ("Second volume is greater.".equalsIgnoreCase(response)) {
            System.out.println("PASS!!!!!");
        } else {
            System.out.println("FAIL!!!!!");
            System.out.println("Expected: Second volume is greater.");
            System.out.println("Response: " + response);
        }
    }
}
