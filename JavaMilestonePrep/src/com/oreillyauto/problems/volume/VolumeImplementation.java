package com.oreillyauto.problems.volume;

class VolumeImplementation extends Volume {
    
    public VolumeImplementation() {
        
    }
    @Override
    void setGallonsAndOunces(int gallons, float ounces) {
        // TODO Auto-generated method stub
        this.gallons = gallons;
        this.ounces = ounces;
    }

    @Override
    int getGallons() {
        // TODO Auto-generated method stub
        return this.gallons;
    }

    @Override
    float getOunces() {
        // TODO Auto-generated method stub
        return this.ounces;
    }

    @Override
    String getVolumeComparison(Volume volume) {
        // TODO Auto-generated method stub
        int v1GallonsToOunces = this.gallons*32;
        int v2GallonsToOunces = volume.gallons*32;
        int v1Total = (int) (v1GallonsToOunces + this.ounces);
        int v2Total = (int) (v2GallonsToOunces + volume.ounces);
        if (v1Total > v2Total) {
            return "First volume is greater";
        }else if (v1Total < v2Total) {
            return "Second volume is greater.";
        }else {
            return "Volumes are equal.";
        }
        
        //return null;
    }

}
