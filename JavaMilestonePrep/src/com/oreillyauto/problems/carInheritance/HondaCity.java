package com.oreillyauto.problems.carInheritance;

public class HondaCity extends Car {
    public Integer mileage = 0;
    public HondaCity(int mileage) {
        super(true, "4");
        this.mileage = mileage;
        // TODO Auto-generated constructor stub
    }
    
    public String getMileage() {
        return this.mileage.toString()+" kmpl";
    }
}
