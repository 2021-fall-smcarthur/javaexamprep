package com.oreillyauto.problems.carInheritance;

public class InnovaCrysta extends Car {
    
    public Integer mileage = 0;
    public InnovaCrysta(int mileage) {
        super(false, "6");
        this.mileage = mileage;
    }
    
    public String getMileage() {
        return this.mileage.toString()+ " kmpl";
    }
}
