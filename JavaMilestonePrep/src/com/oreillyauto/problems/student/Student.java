package com.oreillyauto.problems.student;

class Student {
    public static int counter = 0;
    public String name;
    
    public Student(String name) {
        this.name = name;
        counter+=1;
    }
}
