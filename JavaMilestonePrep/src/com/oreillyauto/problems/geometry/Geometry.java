package com.oreillyauto.problems.geometry;

class Geometry implements Square, Triangle{
    
    public Geometry(){
        
    }
    public int area(int base, int height) {
        return (int) (.5*base*height);
    }
    public int area(int length) {
        return length*length;
    }
}
    
