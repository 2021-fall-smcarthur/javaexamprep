package com.oreillyauto.problems.robot;

import java.util.ArrayList;

class Robot {
    public Integer currentX = 0;
    public Integer currentY = 5;
    public Integer previousX;
    public Integer previousY;
    public String lastMoved;
    public Integer movedAmount = 0;
    
    
    public Robot() {
        
    }
    public Robot(Integer x, Integer y) {
        this.currentY = y;
        this.currentX = x;
        
    }
    public void moveX(Integer dx) {
        previousX = this.currentX;
        previousY = this.currentY;
        //System.out.print("X:"+previousX);
        this.currentX += dx;
        //System.out.print("new X: "+this.currentX+"\n");
        this.lastMoved = "x";
        this.movedAmount = dx;
    }
    public void moveY(Integer dy) {
        previousY = this.currentY;
        previousX = this.currentX;
        //System.out.print("Y: "+previousY);
        this.currentY += dy;
        this.lastMoved = "y";
        this.movedAmount = dy;

    }
    public String printCurrentCoordinates() {
        return currentX.toString()+" "+currentY.toString();
    }
    public String printLastCoordinates() {
        return previousX +" "+previousY;
        }
    public String printLastMove() {
        
            return this.lastMoved+" "+this.movedAmount;
        }
    
}
