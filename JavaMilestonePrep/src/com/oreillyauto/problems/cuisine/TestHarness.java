package com.oreillyauto.problems.cuisine;

/**
 * Julia is the owner of Dine Out, a leading franchise restaurant. The restaurant 
 * serves Chinese, Italian, Japanese, and Mexican cuisines only. Create the 
 * FoodFactory and other required classes to demonstrate the serving process. 
 * 
 * The FoodFactory class should implement the following methods:
 *   - FoodFactory getFactory() to return the instance of the FoodFactory class.
 *   - void registerCuisine(String cuisineKey, Cuisine cuisine) to register the 
 *     cuisine so that the food factory would be able to serve it. Here Cuisine 
 *     is the abstract class.
 *   - Cuisine serveCuisine(String cuisineKey, String dish) to serve a dish for the 
 *     given cuisine. If the requested cuisine is not registered with the food factory, 
 *     then the exception UnservableCuisineRequestException should be thrown with 
 *     the message "Unservable cuisine {cuisineKey} for dish {dish}". 
 *     The implementation of UnservableCuisineRequestException class is already 
 *     provided for you.
 * 
 * The main method (DO NOT MODIFY THE MAIN METHOD!) validates the correctness of the 
 * FoodFactory class implementation by processing some cuisine requests.
 * 
 * - Build the necessary classes to solve this problem
 * - DO NOT use an access modifier for your classes!
 * 
 * Constraints:
 *   1 <= totalNumberOfOrders <= 100
 * 
 * Factory Hint:
 * class UserFactory {
 *   static UserFactory factory; // Singleton instance of this class. Notice it is static!
 *
 *   public static UserFactory getFactory() {
 *       if (factory == null) {
 *           factory = new FoodFactory();
 *       }
 *       return factory;
 *   }
 * }  
 * 
 * @author jbrannon5
 *
 */
public class TestHarness {
    private static StringBuilder sb = new StringBuilder();
    private static final String EXPECTED = "Serving Lasagne(Italian)\n" + 
            "Serving Kamameshi(Japanese)\n" + 
            "Unservable cuisine Polish for dish Marjoram\n";
    private static final FoodFactory FOOD_FACTORY = FoodFactory.getFactory();
    
    static {
        FoodFactory.getFactory().registerCuisine("Chinese", new Chinese());
        FoodFactory.getFactory().registerCuisine("Italian", new Italian());
        FoodFactory.getFactory().registerCuisine("Japanese", new Japanese());
        FoodFactory.getFactory().registerCuisine("Mexican", new Mexican());
    }
    
    public static void main(String[] args) {
        String[] foodArray = new String[] {"Italian Lasagne", "Japanese Kamameshi",
                "Polish Marjoram"};
        
        for (String cuisineDish : foodArray) {
            String[] food = cuisineDish.split(" ");
            String cuisine = food[0];
            String dish = food[1];
            //System.out.print("food: "+dish);

            try {
                if (FOOD_FACTORY.equals(FoodFactory.getFactory())) {
                    Cuisine servedFood = FOOD_FACTORY.serveCuisine(cuisine, dish);
                   // System.out.println(servedFood);

                    switch (cuisine) {
                        case "Chinese":
                            Chinese chineseDish = (Chinese) servedFood;
                            sb.append("Serving " + chineseDish.getDish() + "(Chinese)\n");
                            //System.out.println("Serving " + chineseDish.getDish() + "(Chinese)");
                            break;
                        case "Italian":
                            Italian italianDish = (Italian) servedFood;
                            sb.append("Serving " + italianDish.getDish() + "(Italian)\n");
                            //System.out.println("Serving " + italianDish.getDish() + "(Italian)");
                            break;
                        case "Japanese":
                            Japanese japaneseDish = (Japanese) servedFood;
                            sb.append("Serving " + japaneseDish.getDish() + "(Japanese)\n");
                            //System.out.println("Serving " + japaneseDish.getDish() + "(Japanese)");
                            break;
                        case "Mexican":
                            Mexican mexicanDish = (Mexican) servedFood;
                            sb.append("Serving " + mexicanDish.getDish() + "(Mexican)\n");
                            //System.out.println("Serving " + mexicanDish.getDish() + "(Mexican)");
                            break;
                        default:
                            break;
                    }
                }
                
            } catch (UnservableCuisineRequestException ex) {
                sb.append(ex.getMessage() + "\n");
                //System.out.println(ex.getMessage());
            }
        }
        
        System.out.println("Output: \n" + sb.toString());
        System.out.println("Expected: \n" + EXPECTED);       
        
        if (EXPECTED.equalsIgnoreCase(sb.toString())) {
            System.out.println("1/1 Scenarios Passed");
        } else {
            System.out.println("0/1 Scenarios Passed");
        }
    }
}
