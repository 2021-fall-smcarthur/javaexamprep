package com.oreillyauto.problems.cuisine;

import java.util.HashMap;
import java.util.Map;

public class FoodFactory{
    public static FoodFactory factory;
    public static Map<String, Cuisine> registeredCuisine = new HashMap<String, Cuisine>();
    public static FoodFactory getFactory() {
        if (factory == null) {
            factory = new FoodFactory();
        }
        return factory;
    }
    
    public void registerCuisine(String cuisineKey, Cuisine dish) {
        //System.out.println(dish.toString());
        FoodFactory.registeredCuisine.put(cuisineKey, dish);
    }
    
    public Cuisine serveCuisine(String cuisineKey, String dish) throws UnservableCuisineRequestException {
        if (registeredCuisine.containsKey(cuisineKey)) {  
            registeredCuisine.get(cuisineKey).serveFood(dish);
            return registeredCuisine.get(cuisineKey);
    }else {
        throw new UnservableCuisineRequestException("Unservable cuisine "+cuisineKey + " for dish "+ dish);
    }
      
}
}
